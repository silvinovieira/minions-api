# Minions Store API

Simple e-commerce application api built with the Serverless framework.

This project was bootstrapped with [Serverless Node.js Starter](https://github.com/AnomalyInnovations/serverless-nodejs-starter).
