import AWS from "aws-sdk";
var postmark = require("postmark");
import {mailBcc, mailCc, mailFrom, serverToken} from "./postmark-config";

export async function list(event, context) {
  const params = {
    TableName: process.env.TABLE_NAME,
  };

  function call(action, params) {
    const dynamoDb = new AWS.DynamoDB.DocumentClient();
    return dynamoDb[action](params).promise();
  }

  try {
    const result = await call("scan", params);
    return success(result.Items);
  } catch (e) {
    return failure({ status: false });
  }
}

export function email(event, context, callback) {
  const client = new postmark.ServerClient(serverToken);
  const data = JSON.parse(event.body);
  const date = new Date();
  const mail = {
    to: data.email,
    text: `
      A reserva do seu minion foi realizada com sucesso.
      Detalhes do seu pedido:    
      - Minion: ${data.minionTitle}
      - Descrição: ${data.minionDescription}
      - Data: ${date}
    `
  };

  client.sendEmail({
    "From": mailFrom,
    "To": mail.to,
    "Cc": mailCc,
    "Bcc": mailBcc,
    "Subject": "Pedido de minion recebido",
    "TextBody": mail.text
  }).then(() => {
    callback(null, success({ message: 'Success' }));
  }, err => {
    console.error('Error sending email: ', err.message);
    callback(null, failure({ message: 'Server error' }));
  });
}

function success(body) {
  return buildResponse(200, body);
}

function failure(body) {
  return buildResponse(500, body);
}

function buildResponse(statusCode, body) {
  return {
    statusCode: statusCode,
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Credentials": true
    },
    body: JSON.stringify(body)
  };
}
